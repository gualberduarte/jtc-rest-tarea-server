package py.com.jtc.rest.repository;

import java.util.List;

import py.com.jtc.rest.bean.Empleado;

public interface EmpleadoRepository {

	List<Empleado> obtenerEmpleados();
	
	Empleado obtenerEmpleado(Integer id);
	
	Empleado agregarEmpleado(Empleado contacto);
	
	boolean eliminarEmpleado(Integer id);
	
	boolean actualizarEmpleado(Empleado contacto, Integer id);
}
